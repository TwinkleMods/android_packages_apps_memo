package com.cluster.memo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 *
 * @author Dark
 */
public class MemoAdapter extends ArrayAdapter<Memo> {

    private LayoutInflater inflater;
    private List<Memo> memos;

    /**
     * Basic constructor. Takes a list of memos to display
     *
     * @param context
     * @param objects
     */
    public MemoAdapter(Context context, List<Memo> objects) {
        super(context, R.layout.memo, objects);

        inflater = LayoutInflater.from(context);
        this.memos = objects;
    }

    /**
     * Fills the memo layout with the associated information.
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View tmpConvertView = convertView;
        ViewHolder holder;
        if (tmpConvertView == null) {
            tmpConvertView = inflater.inflate(R.layout.memo, null);
            holder = new ViewHolder();
            holder.name = (TextView) tmpConvertView.findViewById(R.id.name);
            holder.date = (TextView) tmpConvertView.findViewById(R.id.date);
            holder.content = (TextView) tmpConvertView.findViewById(R.id.content);
            tmpConvertView.setTag(holder);
        } else {
            holder = (ViewHolder) tmpConvertView.getTag();
        }
        holder.name.setText((CharSequence) memos.get(position).getName());
        holder.date.setText((CharSequence) memos.get(position).getDate());
        String content = memos.get(position).getContent().replace("\n", " ");
        content = content.length() < 250 ? content : content.substring(0, 250);
        holder.content.setText((CharSequence) content);
        return tmpConvertView;
    }

    /**
     * A small functionless class to store the data in
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    private static class ViewHolder {

        TextView name;
        TextView date;
        TextView content;
    }
}