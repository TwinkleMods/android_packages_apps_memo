/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cluster.memo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.*;

import com.cluster.memo.R.id;

/**
 *
 * @author Dark
 */
public class MemoWriter extends Activity {

    private ContextWrapper wrapper = new ContextWrapper(this);
    private EditText etMemo, inputField;
    private String content, filename = "";
    private AlertDialog fileExistsDialog, filenameDialog, saveDialog;

    /**
     * Called when the activity is first created.
     *
     * @param icicle
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.memowriter);
        etMemo = (EditText) findViewById(R.id.etMemo);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            content = extras.getString("content");
            filename = extras.getString("filename");
            etMemo.setText(content);
        }

        initialize();
        
        TextView lable = (TextView) findViewById(R.id.ab_lable);
        if (filename.equals(filename) || filename.isEmpty()) {
        	lable.setText(filename);
        } else {
        	lable.setText(getString(R.string.memo_new));
        }
    }

    /**
     * Called whenever the screen is rotated or closed. Saves the current state
     * of the application's dialog windows.
     *
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      //  savedInstanceState.putString("filename", inputField.getText().toString());
        savedInstanceState.putBoolean("saveDialog", saveDialog == null ? false : saveDialog.isShowing());
      //  savedInstanceState.putBoolean("filenameDialog", filenameDialog == null ? false : filenameDialog.isShowing());
      //  savedInstanceState.putBoolean("fileExistsDialog", fileExistsDialog == null ? false : fileExistsDialog.isShowing());
      //  if (saveDialog != null) {
      //      saveDialog.dismiss();
      //  }
      //  if (filenameDialog != null) {
      //      filenameDialog.dismiss();
      //  }
      //  if (fileExistsDialog != null) {
      //      fileExistsDialog.dismiss();
       // }
        //super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Called whenever the screen is rotated or closed. Reloads the current
     * state of the application's dialog windows. Called after
     * onSaveInstanceState.
     *
     * @param savedInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        filename = savedInstanceState.getString("filename");
        if (savedInstanceState.getBoolean("saveDialog")) {
            showSaveDialog();
        } else if (savedInstanceState.getBoolean("filenameDialog")) {
            showFilenameDialog();
        } else if (savedInstanceState.getBoolean("fileExistsDialog")) {
            showFileExistsDialog(filename);
        }
    }

    /**
     * Initializes the save button
     */
    private void initialize() {
        final Button btnSave = (Button) findViewById(R.id.btnSave);
        final Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(true);
        btnSave.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                showFilenameDialog();
            }
        });
        
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        etMemo.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // Empty on purpose
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // Empty on purpose
            }

            public void afterTextChanged(Editable arg0) {
                btnSave.setEnabled(true);
                etMemo.removeTextChangedListener(this);
            }
        });
    }

    /**
     * Called when the back button is pressed. This function either shows a save
     * dialog or closes the current view. Result depends on wether the content
     * was changed
     */
    @Override
    public void onBackPressed() {
        String curText = etMemo.getText().toString();
        if (curText.equals(content) || curText.isEmpty()) {
            super.onBackPressed();
            overridePendingTransition(R.anim.fade_in, R.anim.dock_top_exit);
        } else {
            showSaveDialog();
        }
    }

    /**
     * Called when back or save button is pressed. Asks wether or not you want
     * to save the current changes.
     */
    private void showSaveDialog() {
        AlertDialog.Builder alert = getAlertDialog(getString(R.string.save), getString(R.string.memo_save_des));
        alert.setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                showFilenameDialog();
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                finish();
            }
        });

        saveDialog = alert.create();
        saveDialog.show();
    }

    /**
     * Prompts the user to enter a filename to give to the memo.
     */
    private void showFilenameDialog() {
        AlertDialog.Builder alert = getAlertDialog(getString(R.string.memo_name), getString(R.string.memo_name_des));
        inputField = new EditText(this);
        inputField.setText(filename);
        alert.setView(inputField);
        alert.setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                String filename = inputField.getText().toString();
                if (filename.isEmpty()) {
                    dialog.dismiss();
                    Toast.makeText(wrapper, getString(R.string.memo_name_warning), Toast.LENGTH_SHORT).show();
                    showFilenameDialog();
                } else {
                    filename += ".txt";
                }
                File tmpFile = new File(wrapper.getFilesDir().getAbsolutePath() + "/" + filename);
                if (tmpFile != null && tmpFile.isFile()) {
                    showFileExistsDialog(filename);
                } else {
                    if (saveMemo(filename)) {
                        finish();
                    }
                }
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        filenameDialog = alert.create();
        filenameDialog.show();
    }

    /**
     * Called after showFilenameDialog had an error, this happens when the
     * chosen filename is already in use. Prompts the user wether or not to
     * overwrite the existing file.
     */
    private void showFileExistsDialog(final String filename) {
        AlertDialog.Builder alert = getAlertDialog(getString(R.string.memo_fileexist), getString(R.string.memo_fileexist_des));
        alert.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                if (saveMemo(filename)) {
                    finish();
                }
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        fileExistsDialog = alert.create();
        fileExistsDialog.show();
    }

    /**
     * A function that sets the title and message on an AlertDialog.Builder
     * object, this is merely here to reduce redundancy.
     *
     * @param title The title to give to the AlertDialog
     * @param message The message to give to the AlertDialog
     * @return alert The AlertDialog.Builder object containing the title and
     * message
     */
    private AlertDialog.Builder getAlertDialog(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(title);
        alert.setMessage(message);
        return alert;
    }

    /**
     * Saves the current memo text to a file and returns wether or not it
     * succeeded. The file is saved using MODE_WORLD_READABLE to ensure the
     * share functionality works as intended.
     *
     * @param filename The filename the memo should get during the write
     * operation.
     * @return saved A boolean value that indicates wether or not the file was
     * saved succesfully.
     */
    private boolean saveMemo(String filename) {
        boolean saved = false;
        try {
            FileOutputStream stream = openFileOutput(filename, MODE_WORLD_READABLE);
            OutputStreamWriter osw = new OutputStreamWriter(stream);
            osw.write(etMemo.getText().toString() + "d");
            osw.flush();
            osw.close();
            saved = true;
        } catch (FileNotFoundException ex) {
            Log.e(MemoWriter.class.toString(), ex.getMessage());
        } catch (IOException ex) {
            Log.e(MemoWriter.class.toString(), ex.getMessage());
        }
        return saved;
    }
}
