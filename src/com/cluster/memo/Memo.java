/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cluster.memo;

import java.io.File;

/**
 *
 * @author Dark
 */
public class Memo {

    private File file;
    private String name, date, content;

    /**
     * The constructor to create a file from scratch
     *
     * @param file
     * @param name
     * @param date
     * @param content
     */
    public Memo(File file, String name, String date, String content) {
        this.file = file;
        this.name = name;
        this.date = date;
        this.content = content;
    }

    /**
     * Returns the fileobject associated with this memo
     *
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * Sets the fileobject associated with this memo, probably shouldn't be used
     *
     * @param file the name to set
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Returns the filename associated with this memo
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the filename associated with this memo. Can't think of an usage
     * scenario. Probably shouldn't be used
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the last modified date and time
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Returns the date and time this memo was last modified on. Can't think of
     * an usage scenario. Probably shouldn't be used
     *
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the content of the memo. A very long string seperated by \n
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the content of the memo. A very long string seperated by \n
     *
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }
}
