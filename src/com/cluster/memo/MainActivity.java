package com.cluster.memo;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Dark
 */
public class MainActivity extends Activity {

    private static Context context;
    private ContextWrapper wrapper = new ContextWrapper(this);
    private ArrayList<Memo> memos;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        MainActivity.context = this;

        initialize();
    }

    /**
     * Called when the activity is resumed from background (sleepstate)
     * Reinitializes the view
     */
    @Override
    protected void onResume() {
        super.onResume();
        initialize();
    }

    /**
     * Set the click event on the "New memo" button Read current memos and put
     * them in a ListView
     */
    private void initialize() {
        ImageButton btnNew = (ImageButton) findViewById(R.id.btnNew);
        btnNew.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent newMemo = new Intent(wrapper.getApplicationContext(), MemoWriter.class);
                startActivity(newMemo);
                overridePendingTransition(R.anim.dock_top_enter, R.anim.recent_exit);
            }
        });

        File dataFolder = wrapper.getFilesDir();
        memos = new ArrayList<Memo>();
        for (File memoFile : dataFolder.listFiles()) {
            if (memoFile.getName().contains(".txt")) {
                Memo tmpMemo = new Memo(memoFile,
                        memoFile.getName().split(".txt")[0],
                        new Date(memoFile.lastModified()).toLocaleString(),
                        getContents(memoFile));
                memos.add(tmpMemo);
            }
        }

        ListView lv = (ListView) findViewById(R.id.listMemo);

        lv.setAdapter(new MemoAdapter(this, memos));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Intent editMemo = new Intent(context, MemoWriter.class);
                editMemo.putExtra("filename", memos.get(arg2).getName());
                editMemo.putExtra("content", memos.get(arg2).getContent());
                startActivity(editMemo);
                overridePendingTransition(R.anim.dock_top_enter, R.anim.recent_exit);
            }
        });
        registerForContextMenu(lv);
    }

    /**
     * Make a menu when we long press an item from the ListView
     *
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listMemo) {
            menu.add(ContextMenu.NONE, 1, ContextMenu.NONE, getString(R.string.memo_open));
            menu.add(ContextMenu.NONE, 2, ContextMenu.NONE, getString(R.string.memo_share));
            menu.add(ContextMenu.NONE, 3, ContextMenu.NONE, getString(R.string.memo_delete));
        }
    }

    /**
     * Detect which action to take when we click a button in the ContextMenu
     *
     * @param item
     * @return false to allow normal contextmenu processing
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Memo targetMemo = memos.get(info.position);
        switch (item.getItemId()) {
            case 1: // Edit
                Intent editMemo = new Intent(context, MemoWriter.class);
                editMemo.putExtra("filename", targetMemo.getName());
                editMemo.putExtra("content", targetMemo.getContent());
                startActivity(editMemo);
                break;
            case 2: // Share
                share(targetMemo);
                break;
            case 3: // Delete
                deleteFile(targetMemo);
                break;
        }
        return false;
    }

    /**
     * Give the memo to a sharing intend so we can share the file with other
     * applications
     */
    private void share(Memo memo) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri memoUri = Uri.parse("file://" + memo.getFile().getPath());
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, memoUri);
        startActivityForResult(Intent.createChooser(sharingIntent, getString(R.string.memo_share_des)), 1);
    }

    /**
     * Does exactly what you think it would do...deletes the file
     */
    private void deleteFile(Memo memo) {
        boolean deleted = memo.getFile().delete();
        Toast.makeText(context,
                getString(R.string.toast_deleted),
                Toast.LENGTH_LONG).show();
        onCreate(null);
        onRestart();
    }

    /**
     * Reads the file's content for further usage
     *
     * @return memoContent
     */
    private String getContents(File memo) {
        String memoContent = "";
        try {
            DataInputStream in = new DataInputStream(openFileInput(memo.getName()));
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                memoContent += strLine + "\n";
            }
            memoContent = memoContent.substring(0, memoContent.length() - 2);
        } catch (FileNotFoundException ex) {
            Log.e(getClass().getName(), ex.getMessage());
        } catch (IOException ex) {
            Log.e(getClass().getName(), ex.getMessage());
        }
        return memoContent;
    }
}
